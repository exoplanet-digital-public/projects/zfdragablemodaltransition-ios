// swift-tools-version: 5.6

import PackageDescription

let package = Package(
    name: "ZFDragableModalTransition",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "ZFDragableModalTransition",
            targets: ["ZFDragableModalTransition"]
        ),
    ],
    targets: [
        .target(
            name: "ZFDragableModalTransition",
            path: "ZFDragableModalTransition",
            publicHeadersPath: ".",
            cSettings: [
                .headerSearchPath("."),
            ]
        )
    ]
)
